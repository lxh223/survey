\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\thispagestyle {empty}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Preliminaries}{8}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}title1}{8}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}title2}{11}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}title3}{12}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}title4}{13}{section.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Paper collection}{14}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Survey Scope}{14}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Paper Collection Methodology}{15}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Collection Result}{16}{section.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Paper Organisation}{16}{section.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{1) Literature Review.}{17}{paragraph*.9}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{2) Statistical Analysis and Summary.}{17}{paragraph*.10}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Summary of parameter and Hyperparameter adjustment methods}{19}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Hyperparameter Initialization Method}{19}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Activation Function}{20}{subsection.4.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Sigmoid/Logistic Sigmoid Function}{20}{paragraph*.13}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{ReLU Function}{21}{paragraph*.14}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Parametric ReLU Function}{21}{paragraph*.15}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Weight Initialization}{22}{subsection.4.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Xavier Initialization}{22}{paragraph*.16}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Kaming He Initialization}{23}{paragraph*.17}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Conclusion}{23}{subsection.4.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Hyperparameter Optimization Method}{23}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Naive Algorithm}{24}{subsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Grid Search}{25}{paragraph*.19}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Random Search}{26}{paragraph*.21}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Bayes-based Algorithm}{28}{subsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Metaheuristics algorithm}{33}{subsection.4.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Simulated annealing}{33}{paragraph*.22}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Evolutionary algorithm}{34}{paragraph*.23}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Particle swarm optimization}{36}{paragraph*.24}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Ant Colony Algorithm}{38}{paragraph*.25}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Population-based Training}{38}{paragraph*.26}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}Multi-fidelity Model}{39}{subsection.4.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Learning Curve}{40}{paragraph*.27}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Bandit Algorithm}{42}{paragraph*.28}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.5}Other methods}{44}{subsection.4.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Gradient-based optimization}{44}{paragraph*.29}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Hybrid Methodology}{45}{paragraph*.31}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Reinforcement Learning}{46}{paragraph*.32}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Comparison of hyperparameter optimization methods}{48}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Pros and cons of hyperparameter optimization algorithms}{48}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Naive algorithm}{48}{subsection.5.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Grid Search}{48}{subsubsection*.33}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Random search}{49}{subsubsection*.34}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Bayes-based algorithm}{49}{subsection.5.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bayesian optimization-Gaussian process}{49}{subsubsection*.35}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bayesian optimization-Random forest}{50}{subsubsection*.36}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bayesian optimization-Tree parzen estimates}{50}{subsubsection*.37}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.3}Metaheuristics algorithm}{50}{subsection.5.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Genetic algorithm}{50}{subsubsection*.39}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Particle swarm optimization}{51}{subsubsection*.41}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Population-based training}{51}{subsubsection*.42}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.4}Multi-fidelity Model}{52}{subsection.5.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bandit algorithm}{52}{subsubsection*.43}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Performance on specific objective}{52}{section.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Ability to parallelize}{54}{subsection.5.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Multi-objective}{54}{subsection.5.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.3}Scalabiity}{55}{subsection.5.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Existing Hyperparameter Optimization Frameworks}{57}{chapter.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Naive algorithms}{57}{section.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Bayes-based algorithms}{58}{section.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{HyperOpt}{58}{paragraph*.53}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{BayesOpt}{58}{paragraph*.56}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{GPflowOpt}{58}{paragraph*.57}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Skopt}{59}{paragraph*.59}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Optuna}{59}{paragraph*.60}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Hybrid algorithms}{59}{section.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Google Vizier}{59}{paragraph*.61}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Autotune}{60}{paragraph*.62}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Ray Tune}{60}{paragraph*.63}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Statistical Analysis}{62}{chapter.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Research Distribution}{62}{section.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Timeline}{62}{section.7.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}Datasets used in hyperparameter optimization}{63}{section.7.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Current Issues, Open Challenges, and Future Research Directions }{65}{chapter.8}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1}Search Space}{66}{section.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.2}Benchmarks and Comparability}{66}{section.8.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.3}Overfitting and Generalization}{67}{section.8.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.4}Scalability}{67}{section.8.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.5}Continuous updating capability}{68}{section.8.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.6}Neural network initialization}{68}{section.8.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acknowledgements}{70}{chapter*.75}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{References}{71}{chapter*.76}%
