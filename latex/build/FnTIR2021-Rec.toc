\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\thispagestyle {empty}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Preliminaries}{8}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Paper collection}{14}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Survey Scope}{14}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Paper Collection Methodology}{15}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Collection Result}{16}{section.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Paper Organisation}{16}{section.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{1) Literature Review.}{17}{paragraph*.12}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{2) Statistical Analysis and Summary.}{17}{paragraph*.13}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Summary of parameter and Hyperparameter adjustment methods}{19}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Hyperparameter Initialization Method}{19}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Activation Function}{20}{subsection.4.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Sigmoid/Logistic Sigmoid Function}{20}{paragraph*.16}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{ReLU Function}{21}{paragraph*.17}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Parametric ReLU Function}{21}{paragraph*.18}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Weight Initialization}{22}{subsection.4.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Xavier Initialization}{22}{paragraph*.19}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Kaming He Initialization}{23}{paragraph*.20}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Conclusion}{23}{subsection.4.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Hyperparameter Optimization Method}{23}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Black box optimization}{24}{subsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Grid Search}{25}{paragraph*.22}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Random Search}{26}{paragraph*.25}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Bayesian Optimization}{28}{paragraph*.26}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Population-based Algorithm}{33}{paragraph*.28}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Multi-fidelity Model}{38}{subsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Learning Curve}{38}{paragraph*.29}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Bandit Algorithm}{40}{paragraph*.30}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Other methods}{42}{subsection.4.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Gradient-based optimization}{43}{paragraph*.31}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Hybrid Methodology}{44}{paragraph*.33}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Comparison of hyperparameter optimization method}{47}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Pros and cons of hyperparametric optimization algorithm}{47}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Naive algorithm}{47}{subsection.5.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Grid Search}{47}{subsubsection*.34}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Random search}{48}{subsubsection*.35}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Bayes-based algorithm}{48}{subsection.5.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bayesian optimization-Gaussian process}{48}{subsubsection*.36}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bayesian optimization-Random forest}{48}{subsubsection*.37}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bayesian optimization-Tree parzen estimates}{49}{subsubsection*.38}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.3}Metaheuristics algorithm}{49}{subsection.5.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Genetic algorithm}{49}{subsubsection*.39}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Particle swarm optimization}{50}{subsubsection*.40}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Population-based training}{50}{subsubsection*.41}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.4}Multi-fidelity Model}{50}{subsection.5.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bandit algorithm}{50}{subsubsection*.42}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Performance on specific objective}{51}{section.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Parallelism}{51}{subsection.5.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Multi-objective}{53}{subsection.5.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.3}Scalabiity}{54}{subsection.5.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Hyperparameter Optimization Framework}{56}{chapter.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Naive algorithm}{56}{section.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Bayes-based algorithm}{57}{section.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{HyperOpt}{57}{paragraph*.46}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{BayesOpt}{57}{paragraph*.47}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{GPflowOpt}{57}{paragraph*.48}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Skopt}{58}{paragraph*.49}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Optuna}{58}{paragraph*.50}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Hybrid algorithm}{58}{section.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Google Vizier}{58}{paragraph*.51}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Autotune}{59}{paragraph*.52}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Ray Tune}{59}{paragraph*.53}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Current Issues, Challenges, and Future Research Directions }{61}{chapter.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}statistic analysis}{61}{section.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Future work}{62}{section.7.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.1}Benchmarks and Comparability}{62}{subsection.7.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.2}Overfitting and Generalization}{62}{subsection.7.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.3}Scalability}{63}{subsection.7.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.4}Continuous updating capability}{64}{subsection.7.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acknowledgements}{65}{chapter*.58}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{References}{66}{chapter*.59}%
