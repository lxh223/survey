% !TEX root = ../FnTIR2021-Rec.tex

\chapter{Comparison of hyperparameter optimization methods}
\label{sec:othersmethods}

In this chapter, we will introduce the advantages, disadvantages, and versions of the hyperparameter optimization algorithm introduced 
in the previous section. 
We will give the performance of the algorithm on specific objectives, and discuss the development process of the algorithm as well as the 
common versions today.
We hope a compsrison of the algorithms will provide a clearer understanding of the application of the algorithms.

\section{Pros and cons of hyperparameter optimization algorithms}
\label{sec:proscons}

\subsection{Naive algorithm}

In this section, we will describe the pros and cons of different naive hyperparameter optimization methods, and their current applications.

\subsubsection{Grid Search}

Grid search~\SL{References?} uses a process of exhaustive search, so the algorithm implementation is relatively simple, and it is usually used in low-dimensional parameter spaces. 
This makes grid search have high requirements for the parameter value range. 
When the number of parameters increases, the time complexity of grid search will increase geometrically.
Thus, it can only play a role when the parameter value is limited. 
Moreover, with a grid search, it is not able to detect the global optimum of continuous parameters. 
When the dimension of the parameters increases, the complexity of the grid search will increase exponentially. 
Since grid search is exhaustive, it has high resource and time demands.


\subsubsection{Random search}

Random search \citep{bergstra2012random} uses random extraction in place of an exhaustive search, which makes random search more efficient. 
Random search is applicable to almost all parameter types. For deep learning, random search also enables parallelism. 
Since the search process of random search is independent in each iteration, any given instance does not use the previous sampling configuration, 
which makes it difficult to find the global optimal in the search process.



\subsection{Bayes-based algorithm}

Bayesian optimization \citep{frazier2018tutorial} is an excellent algorithm to factor in prior search experience. 
It uses information from the previous sampling configuration in each iteration, which makes results more trustworthy.
Major drawbacks of Bayesian optimization are the inability to parallelize directly, and poor scalability for high-dimensional spaces. 
The Bayesian optimization method can be divided into three parts according to the different surrogate models: BO-GP, SMAC, TPE.

\subsubsection{Bayesian optimization-Gaussian process}	

Bayesian optimization \citep{williams1996gaussian} based on Gaussian process is a classic algorithm. Due to its simplicity and practicality, BO-GP method is widely used in 
machine learning. 
However, it is mostly suitable for continuous and discrete hyperparameters rather than supporting conditional hyperparameters and categorical 
hyperparameters.

\subsubsection{Bayesian optimization-Random forest}

The SMAC~\SL{The full name of Smac is used in Chapter 4} algorithm \citep{hutter2011sequential} uses random forests as a surrogate model. It works well for discrete-valued inputs and can be used for almost all hyperparameter types.
It can handle the hard problems of conditional hyperparameters and categorical hyperparameters.
However, the difficulty of parallelization and poor performance in high-dimensional space are the main constraints.

\subsubsection{Bayesian optimization-Tree parzen estimates}

Tree Parzen Estimators algorithm \citep{bergstra2011algorithms} and the SMAC algorithm both use the tree model, so they have similar characteristics of parallelism and high-dimensional space.
Being available for almost all hyperparameter types is also an advantage.

\subsection{Metaheuristics algorithm}

The metaheuristic algorithm \citep{gogna2013metaheuristics} contains many classifications. Almost all of the algorithms have good performance in parallelism and scalability, which make them suitable for the study of hyperparameter optimization problems in deep learning. \todo{has been rewritten}
In this section, we will introduce those advanced metaheuristics and describe their advantages, disadvantages, and developed versions.

\subsubsection{Genetic algorithm}

%Genetic algorithm is very simple in concept. 
Genetic algorithms \citep{whitley1994genetic} update the population through mutation and combination to obtain better parameter configurations.
Due to this mutation mechanism, genetic algorithms do not easily achieve local optimization; it better reflects global search performance.
Genetic algorithms also have good scalability and is easy to combine with other technologies \citep{mahfoud1995parallel,pelikan1999boa}.
The research on parallelization of genetic algorithm is very suitable for large-scale parallel computers \todo{has been rewritten}. However, premature convergence is a problem in almost all genetic algorithms. 
Researchers generally increase diversity through selection pressure to avoid falling into local optimality. 
The introduction of the two hyperparameters of crossover and mutation operators is another problem of genetic algorithm. 
Whether it can generate adaptive crossover and mutation operators will be the next research direction \citep{katoch2020review}. 


\subsubsection{Particle swarm optimization}


In the particle swarm optimization algorithm, the movement of a particle is not only affected by its locally most famous position, but also by 
the most famous position recognized by other particles in the swarm.
The particle swarm optimization algorithm is very sensitive to the position of the initial particle and the maximum particle swarm velocity, 
while the inertial weight and acceleration coefficient are very sensitive to the particle swarm velocity \citep{wang2019influence}. 
If the population is not properly initialized, the algorithm is very likely to fall into a local optimum \citep{sengupta2019particle}. 
However, the excellent performance in parallelization \citep{chu2005parallel} and flexibility makes the particle swarm optimisaion algorithm very 
suitable in the field of hyperparameter optimization.


\subsubsection{Population-based training}

The population-based training \citep{jaderberg2017population} combines the parallel search method and the sequential optimization method. 
Each training run will periodically perform asynchronous evaluations of its performance, and replace the poorly performing models. 
Moreover, it utilizes the information sharing of concurrently running optimization processes in the group, and allows online transfer 
of parameters and hyperparameters between performance-based group members so that parameters and hyperparameters can be trained together. 
This makes the learning speed much faster. However, most of the implementation of population-based training adopts the glass box method, 
which brings additional restrictions to the realization of the neural network model \citep{li2019generalized}.

\subsection{Multi-fidelity Model}

In this section, we introduce the current versions of multi-precision models and describe the pros and cons.

\subsubsection{Bandit algorithm}

Multi-Armed Bandits algorithm \citep{slivkins2019introduction} incorporates an early stopping strategy based on the study of the learning curve. 
The early stopping strategy allows the bandit algorithm to terminate poorly performing experiments early.
The successive halving method is based on the non-stochastic best-arm identification problem \citep{karnin2013almost}. \todo{The explanation appears in Chapter 4}
It has good robustness and versatility.
But successive halving algorithms suffer from the  "$n\quad vs.\quad B/n$" problem.
The hyperband algorithm \todo{The explanation appears in Chapter 4} can solve such problems very well.
Hyperband has strong real-time performance, flexibility and scalability for high-dimensional spaces, but because it has not learned from the 
previously sampled configuration, it may produce poor results.
The BOHB  algorithm improves this part of the problem. %\todo{define?}
It combines Bayesian optimization and hyperband to enable the algorithm to have good robustness and versatility, as well as parallelism. 
The BOHB algorithm has strong performance and converges to a better configuration in advance than TPE and SMAC.
But it also has shortcomings.The algorithm needs to define a budget before using BOHB algorithm. 
A small budget should produce a cheap approximation for an expensive objective function. 
Misleading or noisy assessments of small budgets can lead to errors in budget allocations, then BOHB's Hyperband Component will be wasted.
\todo{the content has been rewritten}


\begin{table}
	%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{Comparison of hyper-parameter optimization methods}
	\label{tab:1}       % Give a unique label	
	% For LaTeX tables use	
	\resizebox{\textwidth}{!}{
		
		\begin{tabular}{lll}		
			\hline\noalign{\smallskip}		
			\textbf{HPO method} & \textbf{Advantage} & \textbf{Disadvantage}  \\		
			\noalign{\smallskip}\hline\noalign{\smallskip}		
			Grid Search & \tabincell{l}{- Straightforward \\ - Enables parallelization \\ - Reliability in \\low-dimensional space} & \tabincell{l}{- Time-consuming \\ - Poor high-dimensional \\space efficiency}   \\	
			\noalign{\smallskip}\hline
			Random Search & \tabincell{l}{- Enables parallelization \\ - More efficient than\\ grid search} & \tabincell{l}{- No guarantee of \\optimal results \\ - Doesn't use the previous \\sampling configuration}   \\
			\noalign{\smallskip}\hline
			BO-GP & \tabincell{l}{- Supports continuous and \\discrete hyperparameters  \\ - Reliable and promising}& \tabincell{l}{- Not efficient with \\conditional hyperparameters. \\ - Difficult to parallelize \\ - Poor scalability of\\ high-dimensional space }  \\
			\noalign{\smallskip}\hline
			SMAC & \tabincell{l}{- Applicable to almost \\ all hyperparameters} & \tabincell{l}{- Difficult for parallelize \\- Poor scalability of\\ high-dimensional space}  \\
			\noalign{\smallskip}\hline
			TPE & \tabincell{l}{- Applicable to almost \\ all hyperparameters} & \tabincell{l}{- Difficult to parallelize \\- Poor scalability of\\ high-dimensional space}  \\
			\noalign{\smallskip}\hline
			GA & \tabincell{l}{- Applicable to almost \\ all hyperparameters \\ - Can be combined with other\\ algorithms} & \tabincell{l}{- Premature convergence\\- Introduce new hyperparameters }  \\
			\noalign{\smallskip}\hline
			CMA-ES &\tabincell{l}{- Enables parallelization} & - applicability limited   \\
			\noalign{\smallskip}\hline
			PSO & \tabincell{l}{- Enables parallelization \\Applicable to almost \\ all hyperparameters } & \tabincell{l}{- Sensitive to \\initialization }  \\
			\noalign{\smallskip}\hline
			ACO & \tabincell{l}{- Enables parallelization \\ - Can be combined with other\\ algorithms }& \tabincell{l}{- Slow initial \\convergence rate \\ - easy to fall into \\ local optima }  \\
			\noalign{\smallskip}\hline
			PBT & \tabincell{l}{- Combination of parallel search\\ and sequential optimization\\- Enable parallelization \\ } & \tabincell{l}{- Glass box \\implementation}  \\
			\noalign{\smallskip}\hline
			SHA & \tabincell{l}{- Robust and \\versatile} & \tabincell{l}{- $n\quad vs.\quad B/n$ \\problem}  \\
			\noalign{\smallskip}\hline
			HB & \tabincell{l}{- Flexibility and \\scalability of\\ high-dimensional space \\- Enables parallelization} & \tabincell{l}{- Does not use the previous \\sampling configuration}  \\
			\noalign{\smallskip}\hline
			BOHB & \tabincell{l}{- Enables parallelization\\- Robust and \\versatile} & \tabincell{l}{- Misleading small budget }  \\
			\noalign{\smallskip}\hline\noalign{\smallskip}
	\end{tabular}}
\end{table}


\section{Performance on specific objective}
\label{sec:specific}

In this section, we will discuss the performance of hyperparameter optimization algorithms. 
We select three performance criteria for the discussion: (1) ability to parallelize, (2) multi-objective, (3) scalable.
These objectives are crucial for the performance of hyperparameter optimization algorithms.
They may directly affect the scope of application of the algorithm.


\subsection{Ability to parallelize}

Grid search and random search can be easily parallelized.
Since the iterations between different hyperparameter combinations are independent and they do not use prior information, 
grid search and random search is able to run mulitple sets of hyperparameter combinations, provided there is sufficient computing capacity.

Bayesian optimization algorithm is a sequential optimization algorithm, and subsequent training needs to use prior distribution, 
so it is difficult to achieve parallelization.
The parallel design of Bayesian optimization focuses more on the parallel expectation acquisition function\citep{zhan2022fast}.\todo{References are provided}
\cite{wang2020parallel} implements parallel Bayesian optimization with Multipoint Expectation Improvement (q-EI) using stochastic 
gradient estimation proposed by \cite{ginsbourger2008multi}.

Metaheuristic algorithms display satisfactory ability to parellelize.
Generally, population-based algorithms can run multiple hyperparameter combinations for training at the same time.
Efficient parallelizing makes them excellent in convergence speed and running time.
This is one of the reasons why they are more used in deep learning.

Successive halving method and hyperband algorithms do not use prior information.
This guarantees their ability to parallelize.
In order to make the algorithm get better results, hyperband algorithm combined with bayesian optimization algorithm enables the algorithm to achieve robustness and versatility on the basis of preserving parallelism. \todo{has been rewritten}

\subsection{Multi-objective}

Naive hyperparameter optimization algorithms have poor ability to conduct multi-objective optimisation.
Whether it is grid search or random search, only a few goals can be guaranteed.
Especially in the case of a trade-off between final performance and computational resource consumption, usually only one target can be optimised.
Due to the simplicity of the algorithm, it is difficult to improve multi-objective optimisation.

Bandit algorithms \citep{slivkins2019introduction} are similar in form to naive hyperparameter optimization algorithms, but they make excellent improvements in the case of multi-objective optimisation.
Successive halving method and hyperband algorithms use the budget mechanism.
By allocating budgets for the evaluation of different hyperparameter combinations, the consumption of computing resources is 
reduced while the final performance is guaranteed.
The BOHB algorithm further guarantees the final performance of the algorithm. 

%\todo{MA: rephrase para below}
\todo{has been rewritten}
The performance of Bayesian optimization algorithms on single-objective problems is excellent. 
However, based on its sequential optimization characteristics, how to perform well on multi-objective problems is still a problem. 
Scaring the performance index to apply the multi-objective setting to the establishment of the acquisition function might be an answer.

Metaheuristic algorithms perform very well in multi-objective.
Some more advanced algorithms such as NSGA-II\cite{chatterjee2018hybrid}, MOEA/D\cite{zhang2007moea} can be better applied.

\subsection{Scalabiity}

Naive hyperparameter optimization algorithms such as grid search and random search do not perform satisfactorily in terms of scalability.
They can only be used with discrete and continuous hyperparameters, which makes their scope of application very limited.
There is little that naive hyperparameter optimization models can do for conditional hyperparameters and categorical hyperparameters.

The performance of Bayesian optimization algorithms is closely related to its surrogate model.
The performance of Gaussian process-based Bayesian optimization algorithms on continuous and discrete hyperparameters is satisfactory, 
but equally unsuitable for conditional and categorical hyperparameters.
Bayesian optimization algorithms based on random forest and tree parzen estimator improve performance on conditional and classification 
hyperparameters, making Bayesian optimization significantly more scalable.

Metaheuristic algorithms \citep{gogna2013metaheuristics} display good ability to scale.
Advanced genetic algorithms, particle swarm optimization, and population-based training are particularly applicable to almost all 
hyperparameter types, making them widely applicable in deep learning.


Successive halving method \citep{jamieson2016non} is similar to the naive hyperparameter optimization algorithms in form. 
Although it incorporate budget and early stop mechanisms, this does not affect its scalability.
The hyperband algorithm guarantees good scalability, but working with conditional hyperparameters is still an important issue.
The BOHB algorithm solves this problem by combining a Bayesian optimization similar to the TPE algorithm, which makes it applicable 
to almost all hyperparameter types.




\begin{table}[h]
	%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{Comparison of characteristics between algorithms}
	\label{tab:char}       % Give a unique label	
	% For LaTeX tables use	
	\resizebox{\textwidth}{!}{
		\begin{threeparttable}
			\begin{tabular}{llllll}		
				\hline\noalign{\smallskip}	
				\textbf{Category} & \textbf{HPO method} & \textbf{Parallelism} & \textbf{Multi-objective} &  \textbf{Scalability}\\		
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\multirow{2}*{Naive algorithm} &Grid Search & \checkmark & \XSolid &  \XSolid  \\
				~ & Random Search & \checkmark & \XSolid & \XSolid \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\multirow{2}*{Bayes-based algorithm} & \tabincell{l}{BO-GP} & \XSolid & \XSolid & \XSolid \\
				~ & SMAC & \XSolid & \XSolid & \checkmark \\
				~ & TPE & \XSolid & \XSolid & \checkmark \\  
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\multirow{3}*{Metaheuristics algorithm}& Genetic algorithm  & \checkmark & \checkmark & \checkmark \\
				~ & Particle swarm optimization & \checkmark & \checkmark &  \checkmark \\
				~ & Population-based training & \checkmark & \checkmark & \checkmark \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\multirow{3}*{Multi-fidelity Model} & SHA & \checkmark & \checkmark & \XSolid \\
				~ & HB & \checkmark & \checkmark & \XSolid \\
				~ & BOHB & \checkmark & \checkmark & \checkmark\\ 
				\noalign{\smallskip}\hline
			\end{tabular}
			
	\end{threeparttable}}
\end{table}