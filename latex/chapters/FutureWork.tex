% !TEX root = ../FnTIR2021-Rec.tex

%\section{Future directions and new perspectives}
\chapter{Current Issues, Open Challenges, and Future Research Directions }
\label{sec:futuredirections}


Although there have been many excellent performance hyperparameter optimisation algorithms and existing practical frameworks, some issues still need to be addressed, and several aspects in this domain could be improved. 
In this chapter, we discuss the open challenges, current research questions, and potential research directions in the future.
We will analyze each problem and predict possible solutions in the future, which are summarized in Table\ref{tab:Description}.

\begin{table}[h]
	%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{The open challenges and future directions of HPO}
	\label{tab:Description}       % Give a unique label	
	% For LaTeX tables use	
	\resizebox{\textwidth}{!}{
		\begin{threeparttable}
			\begin{tabular}{c|c}		
				\hline\noalign{\smallskip}	
				\textbf{Challenges or Future Requirements} & \textbf{Descriptions or Solutions}  \\		
				\noalign{\smallskip}\hline\noalign{\smallskip}
				Search Space\ref{sec:SearchSpace} & HPO methods should reduce execution time on \\
				& large hyperparameter search space \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				Benchmarks and Comparability\ref{sec:Benchmarks} & There should exist a standard set of benchmarks to fairly  \\ 
				& evaluate and compare different optimization algorithms.  \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				Overfitting and Generalization\ref{sec:Overfitting} & The optimal HPs detected by HPO methods should 
				\\ 
				& have generalizability to build efficient models on unseen data.  \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				Scalability\ref{sec:Scalability} & HPO methods should be scalable to multiple libraries or 
				\\ 
				& platforms And applicable to databases of different sizes.\\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				Continuous updating capability\ref{sec:Continuous} &  HPO methods should consider their capacity to detect and \\ 
				& update optimal HP combinations on continuously-updated data. \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				Neural network initialization\ref{sec:initialization} & The training of neural network can be combined with\\ 
				& simple machine learning model to abate its challenge.  \\			
				\noalign{\smallskip}\hline
			\end{tabular}
			
	\end{threeparttable}}
\end{table}
~\SL{It seems that we should point out more future works in this subsections. Are there any other future work? E.g., can we say something related to parameter initialization as future work?}
~\JK{Added future work related to neural network initialization in \ref{sec:initialization}}


\section{Search Space}
\label{sec:SearchSpace}
In many problems to which machine learning algorithms are applied, only a few hyperparameters have significant effects on model performance, and they are the main hyperparameters that require tuning.
However, certain other unimportant hyperparameters may still affect the performance slightly and may be considered to optimize the ML model further, which increases the dimensionality of hyperparameter search space. 
As the number of hyperparameters and their values increase, they exponentially increase the dimensionality of the search space and the complexity of the problems, and the total objective function evaluation time will also increase exponentially. Therefore, it is necessary to reduce the influence of large search spaces on execution time by reducing the number of relevant super parameters or reducing the corresponding search space methods.

\section{Benchmarks and Comparability}
\label{sec:Benchmarks}

Given the breadth of existing hyperparameter optimisation methods, it is necessary to have a good sense of the strengths and 
weaknesses of each method. 
In order to allow for a fair comparison between different hyperparameter optimisation approaches, the community needs to design 
and agree upon a common set of benchmarks that expands over time, as new hyperparameter optimisation variants emerge.
Current efforts in hyperparameter optimisation have already yielded the Hyperparameter Optimization Library (HPOlib) and a 
benchmark collection specifically for Bayesian optimization methods, but these are not sufficient.

Additionaly, the community needs clearly defined metrics. Currently different studies use different metrics.

At present, an important distinction between evaluations is the way they report performance. 
Performance metrics can be taken from either the validation set used for optimization, or on a separate test set. 
The former helps to study the strength of the optimizer in isolation, without the noise that is added in the evaluation when going 
from validation to test set. 
However, some optimizers may lead to more overfitting than others, which can only be diagnosed by using the test set.


\section{Overfitting and Generalization}
\label{sec:Overfitting}
Generalization is another issue with HPO models. Since hyperparameter evaluations are done with a finite number of evaluations in datasets, the optimal hyper-parameter values detected by HPO approaches might not be the same optimums on previously-unseen data. This is similar to over-fitting issues with ML models that occur when a model is closely fit to a finite number of known data points but is unfit to unseen data [128].
Generalization is also a common concern for multi-fidelity algorithms, like Hyperband and BOHB, since they need to extract subsets to represent the entire dataset.

One solution to reduce or avoid overfitting is to use cross-validation to identify a stable optimum that performs best in all or most of the subsets instead of a sharp optimum that only performs well in a singular validation set. 
However, cross-validation increases the execution time several-fold. 
It would be beneficial if methods can better deal with overfitting and improve generalization in future research.

\section{Scalability}
\label{sec:Scalability}
In practice, one main limitation of many existing HPO frameworks is that they are tightly integrated with one or a couple of machine learning libraries, like sklearn and Keras, which restricts them to only work with a single node instead of large data volumes. 
To tackle large datasets, some distributed machine learning platforms have been developed; however, only very few HPO frameworks exist that support distributed ML. 
Therefore, more research efforts and scalable HPO frameworks, like the ones supporting distributed ML platforms, should be developed to support more libraries.

On the other hand, future practical HPO algorithms should have the scalability to efficiently optimize hyperparameters from a small size to a large size, irrespective of whether they are continuous, discrete, categorical, or conditional hyperparameters.
\begin{comment}
\subsection{Overfitting and Generalization}
\label{sec:Overfitting}

\todo{MA: Most of this section is directly extracted from:https://link.springer.com/chapter/10.1007/978-3-030-05318-5_1.This will need rewriting/referncing.}

An open problem in hyperparameter optimisation is overfitting. 
There is only a finite number of data points available to calculate the validation 
loss to be optimized and thereby do not necessarily optimize for generalization to unseen test datapoints. 
Similarly to overfitting a machine learning algorithm to training data, this problem is about overfitting the hyperparameters to the 
finite validation set.

A simple strategy to reduce overfitting is to employ a different shuffling of the training and validation split for each 
function evaluation; 
this was shown to improve generalization performance for support vector machine tuning, both with a holdout and a cross-validation strategy. 

Another possibility is to use a separate holdout set to assess configurations found by hyperparameter optimisation to avoid bias 
towards the standard validation set.

A different approach to combat overfitting might be to find stable optima instead of sharp optima of the objective function. 
The idea is that for stable optima, the function value around an optimum does not change for slight perturbations of the 
hyperparameters, whereas it does change for sharp optima. 
Stable optima lead to better generalization when applying the found hyperparameters to a new, unseen set of datapoints 
(i.e., the test set). 
An acquisition function built around this was shown to only slightly overfit for support vector machine HPO, while regular Bayesian 
optimization exhibited strong overfitting.

Generalization is another issue with HPO models.
Since hyper-parameter evaluations are done with a finite number of evaluations in datasets, the optimal hyper-parameter values 
detected by HPO approaches might not be the same optimums on previously-unseen data. 
This is similar to over-fitting issues with mechine learning models occur when a model is closely fit to a finite number of known 
data points but is unfit to unseen data\citep{cawley2010over}. 

\subsection{Scalability}
\label{sec:Scalability}

\todo{MA: Again a large amount of directly extracted text from:https://link.springer.com/chapter/10.1007/978-3-030-05318-5_1.}
Despite recent successes in multi-fidelity optimization, there are still machine learning problems which have not been directly 
tackled by HPO due to their scale, and which might require novel approaches. 
Here, scale can mean both the size of the configuration space and the expense of individual model evaluations. 
For example, there has not been any work on HPO for deep neural networks on the ImageNet challenge dataset yet, mostly because of 
the high cost of training even a simple neural network on the dataset. 
At present, for small datasets, most of them set their hyperparameters manually. 
And the populationbased approaches have not yet been shown to be applicable to hyperparameter optimization on datasets larger than a 
few thousand data points.
To tackle large datasets, some distributed machine learning platforms, like Apache SystemML\citep{10.14778/3007263.3007279} and 
Spark MLib\citep{meng2016mllib}, have been developed; 
however, only very few HPO frameworks exist that support distributed machine learning.

On the other hand, future practical HPO algorithms should have the scalability to efficiently optimize hyper-parameters from a small 
size to a large size, irrespective of whether they are continuous, discrete, categorical, or conditional hyper-parameters.

\end{comment}

\section{Continuous updating capability}
\label{sec:Continuous}

In practice, many datasets are not stationary and are constantly updated by adding new data and deleting old data. Correspondingly, 
the optimal hyperparameter values or combinations may also change with the changes in data. 
Currently, developing hyperparameter optimisation methods with the capacity to continuously tune hyperparameter values as the data changes has not drawn 
much attention, since researchers and data analysts often do not alter the machine learning model after achieving a currently optimal performance. 
However, since their optimal hyperparameter values would change as data changes, proper approaches should be proposed to achieve continuous updating capability.

\section{Neural network initialization}
\label{sec:initialization}

Despite the flexibility of neural networks, the application of deep learning to studying physics-based problems has been
slow to increase in popularity. 
In part, the limited use of neural networks by nonexperts is due to the difficulty of training an accurate model\citep{humbird2018deep}. 
One of the most common ways to explain this is by citing the exploding and vanishing gradient problem\citep{glorot2010understanding}. 
Essentially, the weights of the network will either lead to exploding or vanishing values with a large number of passes.
Generally, we will use the weight initialization methods to solve this problem. 
We also introduced the relevant content in the middle of this article, but this is still an almost impossible problem for most people.

Several studies have explored the possibility of mapping decision trees and random forests to neural networks\citep{setiono1999mapping,humbird2018deep}.
To create a black-box neural network, the user-friendly features of tree-based models can be combined with the accuracy, flexibility, and scalability of deep neural networks.
A method of mapping a tree to an equivalent double hidden layer (2HL) neural network is proved to be successful\citep{setiono1999mapping}.
Although it is possible to fit any function with a sufficiently wide, shallow neural network\citep{hornik1991approximation}, studies suggest that deep networks often perform better than wide networks with a similar number of neurons\citep{goodfellow2016deep}.
The performance of 2HL models for high dimensional nonlinear regression problems is not satisfactory.
Deep networks can discover nonlinear relationships not discernible with only 2HL networks. 
So researchers proposed an algorithm combining deep neural network and decision tree, the algorithm is called
“deep jointly informed neural networks” (DJINN)\citep{humbird2018deep}.
These models demonstrate high predictive performance for a variety of regression and classification data sets and
display comparable performance to Bayesian hyperparameter optimization at a lower computational cost. 

Deep neural networks are quickly becoming one of the most popular tools in machine learning due to their success at solving a wide range of problems from language translation to image recognition.  
How to solve the difficulty of training neural networks has become a popular problem. 
Combining with decision tree model is a bold and effective attempt. 
However, the decision tree has its limits. 
We need to find new directions for the more complex problems that can be expected in the utfure.



