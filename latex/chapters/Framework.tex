% !TEX root = ../FnTIR2021-Rec.tex

\chapter{Existing Hyperparameter Optimization Frameworks}
\label{sec:framework}

Hyperparameter optimization is a time-consuming process.
Especially for models with a large number of hyperparameters, such as deep learning, the training process 
usually takes dozens of hours. 
\cite{thornton2012auto} was the first to verify the feasibility of using a fully automated 
method \citep{hall2009weka} to select learning algorithms and their hyperparameters. 
Later, many open source tools, which were developed by combining optimization 
algorithms and schedulers, were proposed, . 
This section will discuss some popular open source libraries and frameworks. 
Table \ref{tab:framework} summarizes selected hyperparameter optimization frameworks. 

\section{Naive algorithms}
\label{sec:NaiveFramework}

Scikit-learn (also known as sklearn) is a machine learning library for the Python programming language. 
It has various classification, regression and clustering algorithms, including support vector machine, random forest, 
gradient boosting, and k-means. 

Sklearn provides two general hyperparameter optimization methods: grid search and random search. 
Sklearn uses cross-validation to effectively evaluate the performance of each configuration, and achieves parallel computing.
The library can be found at: \url{http://scikit-learn.sourceforge.net}.

\section{Bayes-based algorithms}
\label{sec:BayesFramework}

\paragraph{HyperOpt}

Hyperopt \citep{bergstra2013hyperopt} is a Python library for Sequential Model-Based Optimization (SMBO) designed to meet the needs of machine learning researchers to perform hyperparameter optimization. 
It provides two hyperparameter optimization methods: random search and tree parzen estimates method%\todo{MA: add expansion}. 
\todo{Gives an explanation}
Hyperopt combined with MongoDB can perform distributed parameter adjustment, which enables hyperopt to achieve parallelism in hyperparameter optimization. 
According to the characteristics of SMBO, hyperopt can handle hyperparameters of real numbers, discrete values, 
condition variables, etc., which greatly improves its applicability. 
Hyperopt is effective in the hyperparameter optimization of deep neural networks and convolutional neural networks for object 
recognition.
Hyperopt can be combined with scikit-learn \citep{pedregosa2011scikit} to form HyperOpt-sklearn \citep{komer2014hyperopt} 
libraries. 
Hyperopt-sklearn uses Hyperopt to describe the search space for possible configurations of scikit-learn components, 
including preprocessing, classification, and regression modules. One of the main design features of this project \todo{Gives an explanation} is to 
provide a familiar interface to scikit-learn users. With minimal changes, hyperparameter search can be applied to existing 
codebases. 
The library can be found at: \url{http://jaberg.github.com/hyperopt} and \url{https://github.com/hyperopt/hyperopt-sklearn}.

\paragraph{BayesOpt}

BayesOpt \citep{martinez2014bayesopt} is a library that uses Bayesian optimization methods to solve nonlinear optimization, 
random bandit, and sequential experimental design problems. 
It utilizes the Gaussian process as a surrogate model and uses prior knowledge to determine the subsequent results.
The library can be found at: \url{https://bitbucket.org/rmcantin/bayesopt/}

\paragraph{GPflowOpt}

GPflowOpt \citep{knudde2017gpflowopt} introduces a new Python framework for Bayesian optimization. 
It uses TensorFlow to make Gaussian process tasks run on the GPU. 
Therefore, the running speed under this framework is faster and more efficient. 
However, the framework still lacks advanced sampling techniques such as batch processing bayesian optimziation \todo{MA: BO-expand?}, and its performance on discrete 
variables needs to be verified. 
The library can be found at: \url{https://github.com/GPflow/GPflowOpt}

\paragraph{Skopt}

Skopt(scikit-optimize) \citep{head2018scikit} is a hyperparameter optimization framework built on the scikit-learn library 
to minimize expensive and noisy black box functions. 
It includes models such as random search, Bayesian search, decision forest, and gradient boosting tree. 
The library can be found at: \url{https://scikit-optimize.github.io/stable/}

\paragraph{Optuna}

Optuna \citep{akiba2019optuna} provides a Bayes-based method for hyperparameter optimization and effective search structuring. 
Optuna has an imperative, define-by-run style API, lightweight, versatile and cross-platform architecture, 
and efficient sampling algorithms and pruning algorithms. Therefore, the code written in Optuna has a high degree of modularity, 
and users of Optuna can also dynamically construct the search space of hyperparameters.
The library can be found at: \url{https://github.com/pfnet/optuna/}.

\section{Hybrid algorithms}
\label{sec:HybridFramework}

\paragraph{Google Vizier}

Google Vizier \citep{golovin2017google} provides an internal Google service for performing black box optimization. 
Its biggest feature is its ease of use. 
Users only need to select a relevant search algorithm and submit a configuration file, and then they will get a suggested 
set of hyperparameters. 
Vizier's modular design makes it easy to support multiple algorithms, including naive algorithm, model-based algorithm, and 
meta-heuristic algorithm. This allows Google Vizier to be applied to many computing scenarios.
Another advantage of Google Vizier is its implementation of transfer learning.
It can use prior knowledge of other users to guide and accelerate the current research. 
This is very useful for in-depth learning.
Additionally, Google Vizier provides a set of benchmark suites to allow users to configure a set of benchmark runs.
In general, Google Vizier is a forerunner in comprehensive HPO services.

The library can be found at: \url{https://github.com/tobegit3hub/advisor}

\paragraph{Autotune}

Autotune \citep{koch2018autotune} provides an automatic, parallel, non-derivative, hyperparameter optimization framework. 
It implements a variety of search algorithms, such as genetic algorithm, generating set search, and bayesian optimization.
The biggest feature is parallelization, which allow multiple models to be trained in parallel.
It shows that parallel training in the model is better than the experimental results obtained by single training. 
In addition, it can be applied to almost all hyperparameter types in black box optimization, which allows for stronger
applicability. 


\paragraph{Ray Tune}

Ray Tune \citep{liaw2018tune} introduces a scalable hyperparameter optimization Python library.
It uses the distributed hyperparameter search of multiple computing nodes, and supports multiple deep learning frameworks, 
such as Pytorch, TensorFlow and Keras.
Ray Tune can be integrated with a wide range of hyperparameter optimization tools, such as Optuna, Hyperopt, Ax, and Nevergrad.
Ray Tune can be selected from the most advanced algorithms, such as Population-Based Training (PBT), BayesOptSearch, or 
Hyperband/Asynchronous successive halving algorithm , making the framework highly efficient.
Ray Tune is available at \url{http://ray.readthedocs.io/en/latest/tune.html}. 

\begin{table}
	%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{Paper Query Results}
	\label{tab:framework}       % Give a unique label 
	% For LaTeX tables use  
	\resizebox{\textwidth}{!}{
		\begin{threeparttable}
			\begin{tabular}{lllll}   
				\hline\noalign{\smallskip}    
				\textbf{Framework} & \textbf{Support algorithm} & \textbf{Parallelization} & \textbf{Support DL/ML} &  \textbf{Features} \\   
				\noalign{\smallskip}\hline\noalign{\smallskip}    
				Scikit-learn & Grid search and random search & \Checkmark & ML & - Cross-validation \\
				\noalign{\smallskip}\hline
				HyperOpt & Random Search and BO-TPE & \Checkmark & DL & \tabincell{l}{- Ability to create \\complex parameter spaces\\- Persisting and restarting\\- Parallelization}  \\
				\noalign{\smallskip}\hline
				BayesOpt & \tabincell{l}{Bayesian Optimization with\\ a linear regression model} & \XSolid & ML & - Balance of explore and exploit \\
				\noalign{\smallskip}\hline
				GPflowOpt & BO-GP & \Checkmark & ML & \tabincell{l}{- Parallelization\\- Suitable for GPU} \\
				\noalign{\smallskip}\hline
				Skopt & BO-GP & \XSolid & ML & \tabincell{l}{- Simple and efficient}\\
				\noalign{\smallskip}\hline
				Optuna & Bayes-based method & \Checkmark & DL & \tabincell{l}{- Parallelization\\- Quick visualization\\- Lightweight, versatile, \\and platform-agnostic architecture\\- efficient sampling algorithms \\and pruning algorithms}  \\
				\noalign{\smallskip}\hline
				Google Vizier & \tabincell{l}{- Suitable for many\\ algorithms} & \Checkmark & DL & \tabincell{l}{- Easy to use \\- Early stopping strategy}  \\
				\noalign{\smallskip}\hline
				Ray tune & \tabincell{l}{Suitable for many\\ algorithms} & \Checkmark & DL & \tabincell{l}{- Supports many machine\\ learning frameworks\\- Visualized results\\-  Distributed hyperparameter sweep} \\
				\noalign{\smallskip}\hline
				Autotune & Hybrid Solver Manager & \Checkmark & DL & \tabincell{l}{- Suitable for multiple\\ hyperparameters\\- Parallelization}  \\
				\noalign{\smallskip}\hline
			\end{tabular}
			
	\end{threeparttable}}
\end{table}