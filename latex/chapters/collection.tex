% !TEX root = ../FnTIR2021-Rec.tex

%\section{Beyond Traditional \ac{VAE}-Based Recommendation Methods}
\chapter{Paper collection}
\label{sec:collection}


%This section will introduce our paper collection work, the keywords collected, and the preliminary sorting of the collected papers.
This section introduces the methodolgy used for paper collection, including keywords used to search for papers, and our approach
to sorting and analysing the papers. 

\section{Survey Scope}
\label{sub:scope}

There are many methods to conduct hyperparameter optimization. 
The scope of our paper is to systematically summarize the researches in hyperparameter optimization area and conduct the available 
hyperparameter optimization frameworks.

We followed the inclusion criteria below to determine whether to include a paper in our collection. If a paper satisfied one 
or more of the criteria, we included it in the collection. The aspects of hyperparameter optimization that we focus on 
include hyperparameters, hyperparameter optimization frameworks, and hyperparameter optimization applications.

1) The paper proposes a hyperparameter optimization method or framework/tool.

2) The paper applies proposed hyperparameter optimization method to the machine learning model.

3) The paper proposes a dataset or benchmark specifically designed for hyperparameter optimization. 

4) The paper compares the advantages and disadvantages of hyperparameter optimization methods. 

5) The paper summarizes the research directions/existing shortcomings of hyperparameter optimization.

There are many types and variants of hyperparameter optimization methods. We only discuss those methods as systems under a generic group. 
Special optimization methods with excellent performance will be discussed separately. 
%For long-term optimization methods, we will only give their corresponding references and will not introduce them. The description of 
%hyperparameter optimization methods will pay more attention to their process under the current model. 

\section{Paper Collection Methodology}

To collect papers across as many different research areas as possible, we used keyword searches one by one on popular scientific 
databases such as Google Scholar, IEEE Xplore, and arXiv. 
The keywords used for searching are listed below. 

\begin{itemize}
	\setlength{\itemsep}{0pt}
	\setlength{\parsep}{0pt}
	\setlength{\parskip}{0pt}
	\item Hyperparameter optimization|tuning
	\item Machine learning + hyperparameter optimization|tuning
	\item Deep learning + hyperparameter optimization|tuning
	\item Neural network + hyperparameter optimization|tuning
	\item Grid search + hyperparameter optimization|tuning
	\item Random search + hyperparameter optimization|tuning
	\item Bayesian optimization + hyperparameter optimization|tuning
	\item Evolutionary algorithm + hyperparameter optimization|tuning
	\item Multi-fidelity model + hyperparameter optimization|tuning
	\item Bandit algorithm + hyperparameter optimization|tuning
	\item Reinforcement Learning + hyperparameter optimization|tuning
	
\end{itemize}

Since different authors use different terms for their papers, we will snowball every paper we find to include all research results. 
Papers found in this way will also be considered for inclusion in accordance with the standards set by section \ref{sub:scope}. 

\begin{comment}~\SL{``will send''? We have done that, haven't we?}\end{comment}
Moreover, we send emails to the authors of the cited papers and ask them to send us other papers that they know are related to hyperparameter optimization but have not been included. 
We also asked them to check whether our description about their work in the survey was accurate.

\begin{table}
	%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{Paper Query Results}
	\label{tab:keyword}       % Give a unique label	
	% For LaTeX tables use	
	\resizebox{\textwidth}{!}{
		\begin{threeparttable}
			\begin{tabular}{llll}		
				\hline\noalign{\smallskip}		
				Key Words & Hit & Body\tnote{1} & Snowball\tnote{2}\\		
				\noalign{\smallskip}\hline\noalign{\smallskip}		
				Grid Search + hyperparameter optimization & 15 & 5 & 1 \\
				Random Search + hyperparameter optimization & 53 & 15 & 6 \\
				Bayesian optimization + hyperparameter optimization & 41 & 18 & 9 \\
				Population-based algorithm + hyperparameter optimization & 66 & 29 & 12 \\
				Multi-fidelity + hyperparameter optimization & 11 & 6 & 2 \\
				Learning Curve + hyperparameter optimization & 19 & 10 & 5 \\
				Bandit algorithm + hyperparameter optimization & 27 & 14 & 3 \\
				Reinforcement Learnnig + hyperparameter optimization & 16 & 6 & 2\\
				Machine learning + hyperparameter optimization & 38 & 18 & 4\\	
				Deep learning + hyperparameter optimization & 33 & 16 & 3 \\
				Neural network + hyperparameter optimization & 25 & 12 & 3 \\
				Hyperparameter framework & & & 3 \\
				\noalign{\smallskip}\hline\noalign{\smallskip}	
				Query & - & - & 97 \\
				Snowball & - & - & 48 \\
				Author feedback & - & - & \\
				Overall & & & 149 \\
				\noalign{\smallskip}\hline
			\end{tabular}
			\begin{tablenotes}
				\footnotesize
				\item[1] Number of papers cited in the paper
				\item[2] Cited papers extracted from other papers citation
			\end{tablenotes}
		\end{threeparttable}}
\end{table}

\section{Collection Result}

Table \ref{tab:keyword} shows the details of paper collection results. These papers come from websites such as Google scholar, 
Arxiv, IEEE Xplore, ACM or Springer. Keyword search and snowballing resulted in 109 papers across eleven research areas.

Figure \ref{fig:visual} shows the distribution of papers published in different research venues. Of all the papers, 30.27\% are 
published in computer science, 24.30\% are published in mathematics, and 16.86\% papers are published in engineering.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{img/visual.png}
	\caption{Publication Venue Distribution.}
	\label{fig:visual}
\end{figure}

\section{Paper Organisation}
We present the literature review from two aspects: 
1) a literature review of the collected papers, 
2) a statistical analysis of the collected papers, datasets, and tools. 
The sections and the corresponding contents are presented in Table \ref{tab:review}.

\begin{table}[h]
	%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{Review Schema}
	\label{tab:review}       % Give a unique label	
	% For LaTeX tables use	
	\resizebox{\textwidth}{!}{
		\begin{threeparttable}
			\begin{tabular}{lll}		
				\hline\noalign{\smallskip}	
				\textbf{Classification} & \textbf{Sec} & \textbf{Topic}\\		
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\tabincell{l}{Summary of parameter and \\ Hyperparameter adjustment methods} & \tabincell{l}{\ref{sec:Initialization}\\ \ref{sec:Optimization}} & \tabincell{l}{Hyperparameter Initialization Method\\ Hyperparameter Optimization Method} \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\tabincell{l}{Application of hyperparameter \\ optimization method} & \tabincell{l}{\ref{sec:proscons}\\ \ref{sec:specific}} & \tabincell{l}{Pros and cons of hyperparameter optimization \\ Performance on specific objective}  \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\tabincell{l}{Hyperparameter Optimization \\Framework}& \tabincell{l}{\ref{sec:NaiveFramework} \\ \ref{sec:BayesFramework} \\ \ref{sec:HybridFramework} } & \tabincell{l}{Framework based on naive algorithm \\ Framework based on bayes algorithm \\ Framework based on hybrid algorithm }  \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\tabincell{l}{Statistical Analysis}& \tabincell{l}{\ref{sec:research} \\ \ref{sec:timeline} \\ \ref{sec:dataset} } & \tabincell{l}{Research Distribution \\ Timeline \\ Datasets }  \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\tabincell{l}{Current Issues, Challenges, \\ and Future Research Directions}& \tabincell{l}{\ref{sec:SearchSpace} \\ \ref{sec:Benchmarks}\\ \ref{sec:Overfitting} \\ \ref{sec:Scalability} \\ \ref{sec:Continuous}\\ \ref{sec:initialization}} & \tabincell{l}{Search Space \\Benchmarks and Comparability\\ Overfitting and Generalization \\ Scalability \\ Continuous updating capability\\Neural network initialization}  \\
				\noalign{\smallskip}\hline
			\end{tabular}
		
	\end{threeparttable}}
\end{table}

\paragraph{1) Literature Review.} The papers in our collection are organised and presented from two angles.
Section \ref{sec:mainapproaches} summarizes the methods used in hyperparameter optimization, and analyzes the advantages and 
disadvantages of these methods.
Section \ref{sec:othersmethods} describes the application of hyperparameter optimization methods in traditional machine learning 
models and deep learning models.

%The two aspects have different focuses of ML testing, each of which is a complete organisation of the total collected papers, 
%as a single ML testing paper may fit multiple aspects if being viewed from different angles. \todo{MA: not very clear}

\paragraph{2) Statistical Analysis and Summary.}
We analyse and compare the number of research papers on different hyperparameter optimization methods and machine learning 
structures (classic/deep learning). In section \ref{sec:framework} we will discuss those popular open source libraries or 
frameworks.

In Chapter 7, we also added the current research field distribution, time line, and data set, which can more clearly understand the research of hyperparametric optimization.
The three different angles for presentation of related work as well as the statistical summary, analysis, and comparison,
enable us to observe the research focus, trends, challenges, opportunities, and directions of ML testing. 
These results are presented in Section \ref{sec:futuredirections}.
